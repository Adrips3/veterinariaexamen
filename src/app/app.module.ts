import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Importa AngularFireModule de esta manera
import { AngularFireModule } from '@angular/fire/compat';

// Importa AngularFirestoreModule de esta manera
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';

import { MatTableModule } from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { MascotasComponent } from './mascotas/mascotas.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from './environment/environment';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MascotasComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    AppRoutingModule,
    HttpClientModule,
    MatGridListModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
