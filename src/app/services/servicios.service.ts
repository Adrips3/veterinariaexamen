import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { from, Observable, throwError } from 'rxjs';
import { catchError, concatMap  } from 'rxjs/operators';

interface Snapshot {
  // Define the properties of your Snapshot object.
  // For example:
  id: number;
  data: string;
}

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  private veterinariaCollection: AngularFirestoreCollection<any>;

  constructor(private afs: AngularFirestore) {
    this.veterinariaCollection = this.afs.collection('veterinario');
  }

  getMascotas(): Observable<any[]> {
    return this.veterinariaCollection.valueChanges().pipe(
      catchError(this.handleError)
    );
  }

  borrarMascota(id: string): Observable<void> {
    const docRef = this.veterinariaCollection.doc(id);
    return from(docRef.delete()).pipe( // Convertir la Promesa a Observable con from
      catchError(this.handleError)
    );
  }

  crearMascota(id: string, nuevoNombre: string, nuevaEdad: string, nuevaEspecie: string, nuevaRaza: string): Observable<any> {
    const nuevoNombreObjeto = { id: id, nombre: nuevoNombre, edad: nuevaEdad, especie: nuevaEspecie, raza: nuevaRaza };
    return from(this.veterinariaCollection.doc(id).set(nuevoNombreObjeto)).pipe( // Convertir la Promesa a Observable con from
      catchError(this.handleError)
    );
  }

  editarNombreMascota(id: string, nuevoNombre: string, nuevaEdad: string, nuevaEspecie: string, nuevaRaza: string): Observable<void> {
    console.log(id);
  
    // Verificar si el documento existe antes de intentar actualizarlo
    return this.veterinariaCollection.doc(id).get().pipe(
      concatMap((snapshot) => {
        if (snapshot.exists) {
          // El documento existe, ahora puedes proceder con la actualización
          const nuevoNombreObjeto = { id: id, nombre: nuevoNombre, edad: nuevaEdad, especie: nuevaEspecie, raza: nuevaRaza };
          return from(this.veterinariaCollection.doc(id).update(nuevoNombreObjeto));
        } else {
          // El documento no existe, puedes manejar esto como desees (por ejemplo, lanzar un error)
          return throwError(`El documento con ID ${id} no existe.`);
        }
      }),
      catchError(this.handleError)
    );
  }

  private handleError(error: any): Observable<never> {
    console.error('Error en ServiciosService:', error);
  
    // Agregar estos console.log
    console.log('Código de estado:', error.status);
    console.log('Cuerpo de la respuesta:', error.error);
  
    let errMsg: string;
  
    if (error instanceof ErrorEvent) {
      errMsg = `Ocurrió un error: ${error.message}`;
    } else {
      errMsg = `El servidor devolvió un código de error ${error.status}, el cuerpo fue: ${error.error}`;
    }
  
    console.error(errMsg);
    return throwError(errMsg);
  }
}
