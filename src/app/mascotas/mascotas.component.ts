import { Component, OnInit } from '@angular/core';
import { ServiciosService } from '../services/servicios.service';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { of, Observable } from 'rxjs';

@Component({
  selector: 'app-mascotas',
  templateUrl: './mascotas.component.html',
  styleUrls: ['./mascotas.component.scss']
})

export class MascotasComponent implements OnInit {

  public animales: any[] = [];  // Inicializar como un array vacío
  public animalesOriginal: any[] = []; 
  public obtenerAnimales: any;
  public buscadorAnimales: any;
  public abrirInputs: boolean = false;
  public idParaEditar: string = '0';

  public textoBoton:string = '';
  public saberHueco:string = '0';

  public mostrarAlerta: boolean = false;
  public mensajeAlerta: string = '';
  public colorAlerta: string = '';


  dataSource = new MatTableDataSource<any>(this.animales);
  displayedColumns: string[] = ['id','nombre','edad','especie','raza','editar','borrar'];

  constructor(
    private fb: FormBuilder,
    private servicio: ServiciosService) {}

  ngOnInit(): void {
    this.obtenerMascotas();
    this.buildForm();
  }

  buildForm(){
    this.obtenerAnimales = this.fb.group({
      nombre : [null, Validators.required],
      edad   : [null,Validators.required],
      especie: [null, Validators.required],
      raza   : [null, Validators.required]
    });
    this.buscadorAnimales = this.fb.group({
      mascota : [null],
    });
  }

  obtenerMascotas() {
    this.servicio.getMascotas().subscribe((data) => {
      this.animales = data.filter((item: any) => item !== null);
      this.animalesOriginal = [...this.animales];
      if(this.animales.length === 0){
        this.abrirAlerta('Ingresa una mascota', 'guardado', false);
      }
    });
  }

  filtrarAnimales() {
    const filtro = this.buscadorAnimales.value.mascota.toLowerCase();
    
    if (filtro.trim() === '') {
      // Si el campo de búsqueda está vacío, restaurar la lista original
      this.animales = [...this.animalesOriginal];
    } else {
      // Filtrar la lista de animales según el campo de búsqueda
      this.animales = this.animales.filter(animal =>
        animal.nombre.toLowerCase().includes(filtro) ||
        animal.edad.toString().includes(filtro) ||
        animal.especie.toLowerCase().includes(filtro) ||
        animal.raza.toLowerCase().includes(filtro)
      );
    }
  }
  formularioLleno(obetenDatos:any){
    this.obtenerAnimales = this.fb.group({
      nombre : [obetenDatos.nombre],
      edad   : [obetenDatos.edad],
      especie: [obetenDatos.especie],
      raza   : [obetenDatos.raza]
    });
  }

  seccionInputs(animal:any){
    this.formularioLleno(animal);
    this.idParaEditar = animal.id;
    this.abrirInput('Cambiar datos');
  }

  editarAnimal() {
    const nuevoNombre = this.obtenerAnimales.value.nombre,
      nuevaEdad = this.obtenerAnimales.value.edad,
      nuevaEspecie = this.obtenerAnimales.value.especie,
      nuevaRaza = this.obtenerAnimales.value.raza;
  
    // Inicializa observable con un observable que no hace nada
    let observable: Observable<any> = of(null);
  
    if (this.textoBoton === 'Guardar datos') {
      observable = this.servicio.crearMascota(this.saberHueco.toString(), nuevoNombre, nuevaEdad, nuevaEspecie, nuevaRaza);
      this.abrirAlerta(`La mascota ${nuevoNombre} fue guardada correctamente`, 'guardado', true);
    } else if (this.textoBoton === 'Cambiar datos') {
      observable = this.servicio.editarNombreMascota(this.idParaEditar.toString(), nuevoNombre, nuevaEdad, nuevaEspecie, nuevaRaza);
      this.abrirAlerta(`La mascota ${nuevoNombre} fue editada correctamente`, 'editar', true);
    }
  
    // Suscríbete al observable
    observable.subscribe(() => {
      this.obtenerMascotas();
      this.abrirInputs = false;
    });
  }

  agregarNuevaMascota(){
    this.buildForm();
    this.abrirInput('Guardar datos');
    this.saberHueco = this.obtenerSiguienteId();
  }

  abrirInput(llegada:string){
    this.abrirInputs = true;
    this.textoBoton = llegada;
  }

  abrirAlerta(mensaje:string, clase:string, llegada: boolean){
    this.mostrarAlerta = true;
    this.mensajeAlerta = mensaje;
    this.colorAlerta = clase;
    if(llegada == true){
      setTimeout(() => {
        this.mostrarAlerta = false;
      }, 10000);
    }
  }

  obtenerSiguienteId() {    
    const ultimoId = this.animales.sort((a, b) => b.id - a.id)[0]?.id || 0;
    let agregar = parseInt(ultimoId, 10);
    let cambio = agregar+1;
    return cambio.toString();
  }

  borrarAnimal(animal:any){
    const id = animal.id; // Ajusta esto según la estructura de tu objeto animal
    this.servicio.borrarMascota(id).subscribe(() => {
      this.obtenerMascotas();
      this.abrirAlerta('La mascota fue borrada correctamente', 'borrar', true);
    });
  }
}